class Ip {
  constructor(ip){
    this.octetos = ip.split('.').map(function(item,x) {
      return parseInt(item, 10);
    });
  }
  get octeto1() {  return this.octetos[0];  }
  get octeto2() {  return this.octetos[1];  }
  get octeto3() {  return this.octetos[2];  }
  get octeto4() {  return this.octetos[3];  }
  get toString(){
    return this.octetos[0]+"."+
           this.octetos[1]+"."+
           this.octetos[2]+"."+
           this.octetos[3]
  }
  get next_ip(){
    var ip_tmp = new Ip(this.toString);
    if (ip_tmp.octetos[3] < 255) {
      ip_tmp.octetos[3] = ip_tmp.octetos[3]+1;
    }else if (ip_tmp.octetos[3] === 255) {
      ip_tmp.octetos[2] = ip_tmp.octetos[2]+1;
      ip_tmp.octetos[3] = 0;
    }
    return ip_tmp;
  }
  get last_ip(){
    var ip_tmp = new Ip(this.toString);
    if (ip_tmp.octetos[3] <= 255 && ip_tmp.octetos[3] > 0) {
      ip_tmp.octetos[3] = ip_tmp.octetos[3]-1;
    }else if (ip_tmp.octetos[3] === 0) {
      ip_tmp.octetos[2] = ip_tmp.octetos[2]-1;
      ip_tmp.octetos[3] = 255;
    }
    return ip_tmp;
  }
}

class Subred {
  constructor(ip_red){
    this.id_red = new Ip(ip_red);
    this.first_ip = this.id_red.next_ip;
    this.brodcast = new Ip(ip_red);
    this.last_ip = this.brodcast.last_ip;
    this.mask = "255.255.255.0";
  }
  get id_Red(){  return this.id_red;  }
  get first_Ip(){  return this.first_ip;  }
  get last_Ip(){  return this.last_ip;  }
  get Brodcast(){  return this.brodcast;  }
  get Mask(){  return this.mask;  }

  setBrodcast(){
    
  }


}




var ip_red;
var network_mask = [255,255,255,255];
var subRedes = 0;
var tercerOcteto = crearOcteto(256);
var cuartoOcteto = crearOcteto(1);
var sub_Redes = [];

function crearOcteto(base) {
  //funcion para crear un octeto apartir de un numero base
  var base2 = [];
  var mask = 255;
  for (var i = 0; i < 8; i++) {
    base2.push({'base': base,'mask': mask})
    if (base > 255)
      mask = mask - (base/256);
    else
      mask = mask - base;
    base = base * 2;
  }
  return base2;
}

subredes = function(ip,numero) {
  if (numero > 0 && ip_red != "") {
    subRedes = numero;
    ip_red = ip.split('.').map(function(item,x) {
      return parseInt(item, 10);
    });

    numero_ips = 300;
    
    ipBase_mask = ips_x_subred(numero_ips) //obtenemos el numero de ip proximo al que necesitamos

    if (ipBase_mask.base <= 256) {
      var id_red = ip_red.slice();
      var broadcast = id_red.slice(); 
      mask_tmp = network_mask;
      mask_tmp[3] = ipBase_mask.mask;
      broadcast[3] = ipBase_mask.base - 1;
      var red = {
        id_red : id_red,
        broadcast : broadcast,
        mask : mask_tmp
      }
      sub_Redes.push(red);
     }
    if (ipBase_mask.base > 256 && ipBase_mask.base <= 32768) {
      var id_red = ip_red.slice();
      var broadcast = id_red.slice(); 
      mask_tmp = network_mask;
      mask_tmp[2] = ipBase_mask.mask;
      mask_tmp[3] = 0;
      broadcast[2] = (ipBase_mask.base/256) - 1;
      broadcast[3] = 255;
      var red = {
        id_red : id_red,
        broadcast : broadcast,
        mask : mask_tmp
      }
      sub_Redes.push(red);
      }
    

//     for (var i = 0; i < subRedes; i++) {
//        console.log("subred :: "+i)
//      }
  }else
    console.log("numero inferior");
}

ips_x_subred = function (numero_ips) {
  //funcion para buscar en los octetos el numero de ips necesarios y su mascara
  var encontrado;
  if (numero_ips <= 128) {
    encontrado = cuartoOcteto.filter(function (task) {
      return task.base >= numero_ips+2;
    });
  }
  if (numero_ips > 128 && numero_ips <= 32768) {
    encontrado = tercerOcteto.filter(function(task) {
      return task.base >= numero_ips+2;
    });
  }
  return encontrado[0];
}

function main() {
  //console.log("estamos en el main");
  //console.log(subredes("192.168.0.0",6));
  //console.log(sub_Redes);
  
}